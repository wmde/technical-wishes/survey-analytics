Tools for working with WMDE Technical Wishes user survey results.

2022 Survey
=====
* TWL22-Statistics.ipynb
  * Can be viewed at https://public.paws.wmcloud.org/User:Adamw/TWL22-Statistics.ipynb
* Results page: https://de.wikipedia.org/wiki/Wikipedia:Umfragen/Technische_W%C3%BCnsche_2022_Themenschwerpunkte/Stimmabgaben
